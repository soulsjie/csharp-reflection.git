﻿using IReflectionApi;
using ReflectionApi;
using System;
using System.Windows.Forms;

namespace CsharpReflection
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void DoCall(string AnimalType)
        {
            string dllName = textBox1.Text.Trim();//命名空间.类名,程序集
            Type type = System.Type.GetType(dllName);
            if (type == null)
            {
                label1.Text = "找不到类型：" + dllName;
                return;
            }
            object instance = Activator.CreateInstance(type, null);
            if (instance == null)
            {
                label1.Text = "找不到类型：" + dllName;
                return;
            }
            IAnimal animal = instance as IAnimal;
            label1.Text = animal.Call();
            //if (AnimalType=="Cat")
            //{
            //    Cat cat = instance as Cat;
            //    label1.Text = cat.Call();
            //}
            //if (AnimalType == "Dog")
            //{
            //    Dog dog = instance as Dog;
            //    label1.Text = dog.Call();
            //}
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string animalType = textBox1.Text;
            DoCall(animalType);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
