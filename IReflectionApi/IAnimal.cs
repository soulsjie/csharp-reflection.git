﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IReflectionApi
{
    public interface IAnimal
    {
        /// <summary>
        /// 叫
        /// </summary>
        /// <returns></returns>
        string Call();
    }
}
