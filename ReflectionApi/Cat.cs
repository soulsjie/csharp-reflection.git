﻿using IReflectionApi;
using System.IO;

namespace ReflectionApi
{
    public class Cat : IAnimal
    {
        public Cat()
        {
            name = "Cat";
        }
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Call()
        {
            string nowDateTimeStr = System.DateTime.Now.ToString("yyyyMMdd-HHmmss");
            return nowDateTimeStr + "\r\n" + name + "\r\n" + "喵喵";
        }
    }
}
